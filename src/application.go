package main

import (
	"bisonanon.com/gocraft/global"
	"flag"
	_ "image/png"
)

func init() {
	verbose := flag.Bool("verbose", false, "log debug messages")
	width := flag.Int("width", 1500, "window width")
	height := flag.Int("height", 1000, "window height")
	fullscreen := flag.Bool("fullscreen", false, "windowed fullscreen")
	flag.Parse()
	global.InitLogger(*verbose)
	global.Settings.WindowWidth = *width
	global.Settings.WindowHeight = *height
	global.Settings.Fullscreen = *fullscreen
}

func main() {
	win := NewWindow("GoCraft", global.Settings.WindowWidth, global.Settings.WindowHeight)
	win.Run()
}
