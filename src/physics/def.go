package physics

import (
	"bisonanon.com/gocraft/world/solids"
	"github.com/go-gl/mathgl/mgl32"
)

type Object struct {
	solids.Movable
	Pos mgl32.Vec3
	Vel mgl32.Vec3
	Acc mgl32.Vec3
}

func (o *Object) update(dt float32) {
	o.Vel = o.Vel.Add(o.Acc.Mul(dt))
	o.Pos = o.Pos.Add(o.Vel.Mul(dt))
	o.Movable.Move(o.Pos.X(), o.Pos.Y(), o.Pos.Z())
}

type Manager interface {
	Update(dt float32)
	AddObject(obj *Object)
}

type manager struct {
	objs []*Object
}

func NewManager() Manager {
	return &manager{objs: make([]*Object, 0)}
}

func (m *manager) Update(dt float32) {
	for _, obj := range m.objs {
		obj.update(dt)
	}
}

func (m *manager) AddObject(obj *Object) {
	m.objs = append(m.objs, obj)
}
