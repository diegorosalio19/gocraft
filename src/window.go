package main

import (
	"bisonanon.com/gocraft/global"
	"bisonanon.com/gocraft/view/gui"
	"bisonanon.com/gocraft/world"
	"fmt"
	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/glfw/v3.1/glfw"
	"github.com/go-gl/mathgl/mgl32"
	"github.com/go-gl/mathgl/mgl64"
	"runtime"
	"time"
)

type Window struct {
	winP          *glfw.Window
	world         world.World
	gui           gui.Gui
	showDebugInfo bool
}

var (
	setDebugFPS gui.ValueModifier
	setDebugX   gui.ValueModifier
	setDebugY   gui.ValueModifier
	setDebugZ   gui.ValueModifier
)

func NewWindow(title string, width, height int) Window {
	initGLFW()

	videoMode := glfw.GetPrimaryMonitor().GetVideoMode()

	glfw.WindowHint(glfw.RedBits, videoMode.RedBits)
	glfw.WindowHint(glfw.GreenBits, videoMode.GreenBits)
	glfw.WindowHint(glfw.BlueBits, videoMode.BlueBits)
	glfw.WindowHint(glfw.RefreshRate, videoMode.RefreshRate)
	glfw.WindowHint(glfw.Samples, 4)
	glfw.WindowHint(glfw.Resizable, glfw.False)
	glfw.WindowHint(glfw.ContextVersionMajor, 4)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)

	var monitor *glfw.Monitor = nil
	if global.Settings.Fullscreen {
		width, height = videoMode.Width, videoMode.Height
		monitor = glfw.GetPrimaryMonitor()
	}

	window, err := glfw.CreateWindow(width, height, title, monitor, nil)

	if err != nil {
		panic(err)
	}
	window.MakeContextCurrent()
	initOpengl()
	window.SetInputMode(glfw.CursorMode, glfw.CursorDisabled)

	g := gui.NewGui()
	g.DebugInfos.AddInfo(0, "ms", "0", mgl32.Vec3{1, 1, 1}, gui.Value)
	g.DebugInfos.AddInfo(1, "X", "0", mgl32.Vec3{1, 0, 0}, gui.Value)
	g.DebugInfos.AddInfo(1, "Y", "0", mgl32.Vec3{0, 1, 0}, gui.Value)
	g.DebugInfos.AddInfo(1, "Z", "0", mgl32.Vec3{0, 0, 1}, gui.Value)
	setDebugFPS = g.DebugInfos.ValueModifier("ms")
	setDebugX = g.DebugInfos.ValueModifier("X")
	setDebugY = g.DebugInfos.ValueModifier("Y")
	setDebugZ = g.DebugInfos.ValueModifier("Z")

	return Window{
		winP:  window,
		world: world.NewWorld(),
		gui:   g,
	}
}

var ticker = time.NewTicker(time.Second / 5)

func (w *Window) Run() {
	w.winP.SetCursorPosCallback(w.getMouseCallback())
	w.winP.SetScrollCallback(w.getScrollCallback())
	w.winP.SetKeyCallback(w.getKeyCallback())

	var lastTime = time.Now()
	for !w.winP.ShouldClose() {
		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

		delta := time.Now().Sub(lastTime)
		lastTime = time.Now()

		w.keyboardHandle(delta)

		w.world.Update(delta)
		w.world.Render()
		if w.showDebugInfo {
			select {
			case <-ticker.C:
				setDebugFPS(fmt.Sprintf("%6.2f", mgl64.Clamp(delta.Seconds()*1000, 0, 999)))
			default:
			}
			setDebugX(fmt.Sprintf("%6.2f", w.world.PlayerPos().X()))
			setDebugY(fmt.Sprintf("%6.2f", w.world.PlayerPos().Y()))
			setDebugZ(fmt.Sprintf("%6.2f", w.world.PlayerPos().Z()))
			w.gui.RenderDebugInfos()
		}

		w.winP.SwapBuffers()
		glfw.PollEvents()
	}
	glfw.Terminate()
}

func (w *Window) getMouseCallback() glfw.CursorPosCallback {
	lastX, lastY, firstMouse := new(float64), new(float64), new(bool)
	*firstMouse = true
	return func(_ *glfw.Window, xPos float64, yPos float64) {
		if *firstMouse {
			*lastX = xPos
			*lastY = yPos
			*firstMouse = false
		}
		xOff := xPos - *lastX
		yOff := *lastY - yPos
		*lastX, *lastY = xPos, yPos
		w.world.Cam().Rotate(mgl32.Vec2{float32(xOff), float32(yOff)})
	}
}

func (w *Window) getScrollCallback() glfw.ScrollCallback {
	return func(_ *glfw.Window, _ float64, y float64) {
		if y == 1 {
			w.world.Cam().ZoomOut()
		} else {
			w.world.Cam().ZoomIn()
		}
	}
}

func (w *Window) getKeyCallback() glfw.KeyCallback {
	return func(_ *glfw.Window, key glfw.Key, _ int, action glfw.Action, _ glfw.ModifierKey) {
		if key == glfw.KeyF3 && action == glfw.Press {
			w.showDebugInfo = !w.showDebugInfo
		}
	}
}

func (w *Window) keyboardHandle(elapsedTime time.Duration) {
	var x, y, z float32
	if w.winP.GetKey(glfw.KeyW) == glfw.Press {
		z--
	}
	if w.winP.GetKey(glfw.KeyA) == glfw.Press {
		x--
	}
	if w.winP.GetKey(glfw.KeyS) == glfw.Press {
		z++
	}
	if w.winP.GetKey(glfw.KeyD) == glfw.Press {
		x++
	}
	if w.winP.GetKey(glfw.KeySpace) == glfw.Press {
		y++
	}
	if w.winP.GetKey(glfw.KeyLeftShift) == glfw.Press {
		y--
	}
	if w.winP.GetKey(glfw.KeyR) == glfw.Press {
		w.world.Cam().LockToPoint(mgl32.Vec3{0, 0, 0})
	}
	if w.winP.GetKey(glfw.KeyT) == glfw.Press {
		w.world.Cam().Unlock()
	}
	if w.winP.GetKey(glfw.KeyF) == glfw.Press {
		gl.PolygonMode(gl.FRONT_AND_BACK, gl.LINE)
	}
	if w.winP.GetKey(glfw.KeyG) == glfw.Press {
		gl.PolygonMode(gl.FRONT_AND_BACK, gl.FILL)
	}
	movement := mgl32.Vec3{x, y, z}.Mul(float32(elapsedTime.Seconds()))
	w.world.Cam().Move(movement)
}

func initGLFW() {
	runtime.LockOSThread()
	err := glfw.Init() // terminating missing
	if err != nil {
		panic(err)
	}
}

func initOpengl() {
	if err := gl.Init(); err != nil {
		panic(err)
	}
	version := gl.GoStr(gl.GetString(gl.VERSION))
	global.Logger.Println("OpenGL version", version)
	gl.Enable(gl.DEPTH_TEST)
	gl.Enable(gl.CULL_FACE)
	gl.Enable(gl.MULTISAMPLE)
}
