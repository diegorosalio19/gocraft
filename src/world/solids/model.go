package solids

import (
	"bisonanon.com/gocraft/view"
	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/mathgl/mgl32"
	"unsafe"
)

type Modeller interface {
	view.Mesher
	AddQuad(v0, v1, v2, v3 mgl32.Vec3)
	AddTriangle(v0, v1, v2 mgl32.Vec3)
}

type basicModel struct {
	vertexes [][2]mgl32.Vec3
	indexes  []int32

	cachedMesh *view.Mesh
}

func NewModel() Modeller {
	return &basicModel{}
}

func (m *basicModel) Mesh() view.Mesh {
	if m.cachedMesh != nil {
		return *m.cachedMesh
	}
	var vertexData = make([]float32, 0, len(m.vertexes)*6)
	for _, vertex := range m.vertexes {
		vertexData = append(vertexData, vertex[0].X(), vertex[0].Y(), vertex[0].Z())
		vertexData = append(vertexData, vertex[1].X(), vertex[1].Y(), vertex[1].Z())
	}
	mesh := new(view.Mesh)
	m.cachedMesh = mesh

	mesh.EboStart = 0
	mesh.EboEnd = int32(len(m.indexes))

	gl.GenBuffers(1, &mesh.Vbo)
	gl.GenBuffers(1, &mesh.Ebo)

	gl.GenVertexArrays(1, &mesh.Vao)
	gl.BindVertexArray(mesh.Vao)

	gl.BindBuffer(gl.ARRAY_BUFFER, mesh.Vbo)
	gl.BufferData(gl.ARRAY_BUFFER, len(vertexData)*4, gl.Ptr(vertexData), gl.STATIC_DRAW)

	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, mesh.Ebo)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, len(m.indexes)*4, gl.Ptr(m.indexes), gl.STATIC_DRAW)

	gl.EnableVertexAttribArray(0)
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 6*4, nil)
	gl.EnableVertexAttribArray(1)
	gl.VertexAttribPointer(1, 3, gl.FLOAT, false, 6*4, unsafe.Add(nil, 3*4))
	return *mesh
}

func (m *basicModel) AddQuad(v0, v1, v2, v3 mgl32.Vec3) {
	m.AddTriangle(v0, v2, v3)
	m.AddTriangle(v0, v1, v2)
}

func (m *basicModel) AddTriangle(v0, v1, v2 mgl32.Vec3) {
	n := v1.Sub(v0).Cross(v2.Sub(v0))
	i := int32(len(m.vertexes))
	m.vertexes = append(m.vertexes, [2]mgl32.Vec3{v0, n}, [2]mgl32.Vec3{v1, n}, [2]mgl32.Vec3{v2, n})
	m.indexes = append(m.indexes, i, i+1, i+2)
	m.invalidateCache()
}

func (m *basicModel) invalidateCache() {
	if m.cachedMesh == nil {
		return
	}
	gl.DeleteBuffers(1, &m.cachedMesh.Vbo)
	gl.DeleteBuffers(1, &m.cachedMesh.Ebo)
	gl.DeleteVertexArrays(1, &m.cachedMesh.Vao)
	m.cachedMesh = nil
}

type Rotatable interface {
	Rotate(deg, x, y, z float32)
}

type Scalable interface {
	Scale(x, y, z float32)
}

type Movable interface {
	Move(x, y, z float32)
}

type WorldObject interface {
	view.MatrixModeler
	Rotatable
	Scalable
	Movable
}
