package solids

import "C"
import (
	"github.com/go-gl/mathgl/mgl32"
	"math"
)

func Cube() Modeller {
	var (
		v0 = mgl32.Vec3{-0.5, -0.5, -0.5}
		v1 = mgl32.Vec3{-0.5, -0.5, 0.5}
		v2 = mgl32.Vec3{-0.5, 0.5, 0.5}
		v3 = mgl32.Vec3{-0.5, 0.5, -0.5}
		v4 = mgl32.Vec3{0.5, 0.5, -0.5}
		v5 = mgl32.Vec3{0.5, 0.5, 0.5}
		v6 = mgl32.Vec3{0.5, -0.5, 0.5}
		v7 = mgl32.Vec3{0.5, -0.5, -0.5}
	)

	c := NewModel()
	c.AddQuad(v6, v7, v4, v5)
	c.AddQuad(v7, v0, v3, v4)
	c.AddQuad(v0, v1, v2, v3)
	c.AddQuad(v1, v6, v5, v2)
	c.AddQuad(v2, v5, v4, v3)
	c.AddQuad(v0, v7, v6, v1)
	return c
}

func Pyramid() Modeller {
	var (
		v0 = mgl32.Vec3{-0.5, -0.5, -0.5}
		v1 = mgl32.Vec3{-0.5, -0.5, 0.5}
		v2 = mgl32.Vec3{0.5, -0.5, 0.5}
		v3 = mgl32.Vec3{0.5, -0.5, -0.5}
		v4 = mgl32.Vec3{0, 0.5, 0}
	)

	c := NewModel()
	c.AddQuad(v0, v3, v2, v1)
	c.AddTriangle(v2, v3, v4)
	c.AddTriangle(v3, v0, v4)
	c.AddTriangle(v0, v1, v4)
	c.AddTriangle(v1, v2, v4)
	return c
}

func UVSphere(mNum, pNum int) Modeller {
	c := NewModel()
	for i := 0; i < pNum; i++ {
		phi0 := float32(i) * math.Pi / float32(pNum)
		phi1 := float32(i+1) * math.Pi / float32(pNum)
		for j := 0; j < mNum; j++ {
			theta0 := float32(j) * 2 * math.Pi / float32(mNum)
			theta1 := float32(j+1) * 2 * math.Pi / float32(mNum)
			c.AddQuad(
				mgl32.SphericalToCartesian(1, phi0, theta0),
				mgl32.SphericalToCartesian(1, phi1, theta0),
				mgl32.SphericalToCartesian(1, phi1, theta1),
				mgl32.SphericalToCartesian(1, phi0, theta1),
			)
		}
	}
	return c
}

func Icosahedron() Modeller {
	c := NewModel()
	var (
		short = float32(1) / math.Phi

		vX00 = mgl32.Vec3{-0.5, 0, -short / 2}
		vX01 = mgl32.Vec3{-0.5, 0, +short / 2}
		vX11 = mgl32.Vec3{+0.5, 0, +short / 2}
		vX10 = mgl32.Vec3{+0.5, 0, -short / 2}

		vY00 = mgl32.Vec3{-short / 2, -0.5, 0}
		vY01 = mgl32.Vec3{+short / 2, -0.5, 0}
		vY11 = mgl32.Vec3{+short / 2, +0.5, 0}
		vY10 = mgl32.Vec3{-short / 2, +0.5, 0}

		vZ00 = mgl32.Vec3{0, -short / 2, -0.5}
		vZ01 = mgl32.Vec3{0, -short / 2, +0.5}
		vZ11 = mgl32.Vec3{0, +short / 2, +0.5}
		vZ10 = mgl32.Vec3{0, +short / 2, -0.5}
	)

	c.AddTriangle(vX00, vX01, vY10)
	c.AddTriangle(vX00, vY00, vX01)
	c.AddTriangle(vX11, vX10, vY11)
	c.AddTriangle(vX11, vY01, vX10)

	c.AddTriangle(vY00, vY01, vZ01)
	c.AddTriangle(vY00, vZ00, vY01)
	c.AddTriangle(vY10, vY11, vZ10)
	c.AddTriangle(vY10, vZ11, vY11)

	c.AddTriangle(vZ01, vZ11, vX01)
	c.AddTriangle(vZ01, vX11, vZ11)
	c.AddTriangle(vZ10, vZ00, vX00)
	c.AddTriangle(vZ10, vX10, vZ00)

	c.AddTriangle(vX01, vZ11, vY10)
	c.AddTriangle(vZ11, vX11, vY11)
	c.AddTriangle(vY11, vX10, vZ10)
	c.AddTriangle(vZ10, vX00, vY10)

	c.AddTriangle(vX01, vY00, vZ01)
	c.AddTriangle(vZ01, vY01, vX11)
	c.AddTriangle(vY01, vZ00, vX10)
	c.AddTriangle(vX00, vZ00, vY00)

	return c
}
