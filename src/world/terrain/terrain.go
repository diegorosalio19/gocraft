package terrain

import (
	"bisonanon.com/gocraft/global/utils"
	"bisonanon.com/gocraft/view"
	"bisonanon.com/gocraft/view/material"
	"bisonanon.com/gocraft/world/solids"
	"github.com/go-gl/mathgl/mgl32"
	"github.com/ojrac/opensimplex-go"
	"math/rand"
	"time"
)

const (
	density   = 16
	ChunkSize = 16
	height    = 4
)

type Terrain struct {
	model        solids.Modeller
	vertexes     [density][density]mgl32.Vec3
	modelMatrix  mgl32.Mat4
	normalMatrix mgl32.Mat3
}

func (t *Terrain) Mesh() view.Mesh {
	return t.model.Mesh()
}

func (t *Terrain) ModelMatrix() mgl32.Mat4 {
	return t.modelMatrix
}

func (t *Terrain) NormalMatrix() mgl32.Mat3 {
	return t.normalMatrix
}

func (t *Terrain) Material() material.Material {
	return material.PlasticGreen
}

type Generator struct {
	seed            int64
	randomGenerator *rand.Rand
	noise           opensimplex.Noise32
}

func NewGenerator() Generator {
	seed := time.Now().Unix()
	return Generator{
		randomGenerator: rand.New(rand.NewSource(seed)),
		noise:           opensimplex.NewNormalized32(time.Now().Unix()),
		seed:            seed,
	}
}

func (g *Generator) Generate(position mgl32.Vec3) *Terrain {
	t := Terrain{}
	vertexes := &t.vertexes
	for x := 0; x < density; x++ {
		for z := 0; z < density; z++ {
			vertexes[x][z] = mgl32.Vec3{
				float32(x) / (density - 1),
				g.noise.Eval2(float32(x)/(density-1)+position.X(), float32(z)/(density-1)+position.Z()) * height,
				float32(z) / (density - 1),
			}
		}
	}

	t.model = solids.NewModel()
	for x := 0; x < density; x++ {
		for z := 0; z < density; z++ {
			if utils.InRange(0, x-1, density) && utils.InRange(0, z-1, density) {
				t.model.AddTriangle(vertexes[x][z], vertexes[x][z-1], vertexes[x-1][z])
			}
			if utils.InRange(0, x+1, density) && utils.InRange(0, z+1, density) {
				t.model.AddTriangle(vertexes[x][z], vertexes[x][z+1], vertexes[x+1][z])
			}
		}
	}

	t.modelMatrix = mgl32.Translate3D(position.X()*ChunkSize, 0, position.Z()*ChunkSize).Mul4(mgl32.Scale3D(ChunkSize, 1, ChunkSize))
	t.normalMatrix = t.modelMatrix.Inv().Transpose().Mat3()
	return &t
}
