package world

import (
	"bisonanon.com/gocraft/physics"
	"bisonanon.com/gocraft/view"
	"bisonanon.com/gocraft/view/lights"
	"bisonanon.com/gocraft/world/terrain"
	"github.com/go-gl/mathgl/mgl32"
	"time"
)

type World struct {
	generator      terrain.Generator
	renderManager  view.RenderManager
	physicsManager physics.Manager
	lightManager   lights.Manager

	terrain map[int32]map[int32]*terrain.Terrain
	balls   []*Entity
}

func NewWorld() World {
	w := World{
		generator:      terrain.NewGenerator(),
		renderManager:  view.NewRenderManager(),
		physicsManager: physics.NewManager(),
		lightManager:   lights.NewManger(),
		terrain:        make(map[int32]map[int32]*terrain.Terrain),
		balls:          make([]*Entity, 10),
	}
	w.lightManager.Directions = append(w.lightManager.Directions, lights.Sun)
	for i := 0; i < 10; i++ {
		e := NewEntity()
		e.Scale(2, 2, 2)
		w.physicsManager.AddObject(&physics.Object{
			Movable: &e,
			Pos:     mgl32.Vec3{float32(4 * i), 100, 0},
			Vel:     mgl32.Vec3{0, 0, 0},
			Acc:     mgl32.Vec3{0, -9.81, 0},
		})
		w.balls[i] = &e
	}
	return w
}

func (w *World) PlayerPos() mgl32.Vec3 {
	return w.renderManager.Cam.Pos()
}

func (w *World) Cam() *view.Camera {
	return &w.renderManager.Cam
}

func (w *World) Update(delta time.Duration) {
	w.physicsManager.Update(float32(delta.Seconds()))
	// w.balls[1].Move(0, float32(delta.Seconds()), 0)
}

func (w *World) Render() {
	w.renderManager.SetLights(w.lightManager)

	const renderDistance = 6
	playerPosition := w.PlayerPos()
	chunkX := int32(playerPosition.X()) / terrain.ChunkSize
	chunkZ := int32(playerPosition.Z()) / terrain.ChunkSize
	for x := chunkX - renderDistance; x < chunkX+renderDistance; x++ {
		if _, ok := w.terrain[x]; !ok {
			w.terrain[x] = make(map[int32]*terrain.Terrain)
		}
		zMap := w.terrain[x]
		for z := chunkZ - renderDistance; z < chunkZ+renderDistance; z++ {
			if _, ok := zMap[z]; !ok {
				zMap[z] = w.generator.Generate(mgl32.Vec3{float32(x), 0, float32(z)})
			}
			w.renderManager.Render(zMap[z])
		}
	}

	for _, ball := range w.balls {
		w.renderManager.Render(ball)
	}
}
