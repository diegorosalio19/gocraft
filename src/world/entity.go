package world

import (
	"bisonanon.com/gocraft/view"
	"bisonanon.com/gocraft/view/material"
	"bisonanon.com/gocraft/world/solids"
	"github.com/go-gl/mathgl/mgl32"
)

var ballModel = solids.UVSphere(20, 20)

type Entity struct {
	translate, scale, rotate mgl32.Mat4
	modelMatrix              *mgl32.Mat4
	normalMatrix             *mgl32.Mat3
}

func NewEntity() Entity {
	return Entity{
		translate: mgl32.Ident4(),
		scale:     mgl32.Ident4(),
		rotate:    mgl32.Ident4(),
	}
}

func (e *Entity) invalidateCache() {
	e.normalMatrix = nil
	e.modelMatrix = nil
}

func (e *Entity) Rotate(deg, x, y, z float32) {
	mat := mgl32.HomogRotate3D(mgl32.DegToRad(deg), mgl32.Vec3{x, y, z}.Normalize())
	e.rotate = e.rotate.Mul4(mat)
	e.invalidateCache()
}

func (e *Entity) Scale(x, y, z float32) {
	mat := mgl32.Scale3D(x, y, z)
	e.scale = e.scale.Mul4(mat)
	e.invalidateCache()
}

func (e *Entity) Move(x, y, z float32) {
	mat := mgl32.Translate3D(x, y, z)
	e.translate = mat
	e.invalidateCache()
}

func (e *Entity) Mesh() view.Mesh {
	return ballModel.Mesh()
}

func (e *Entity) ModelMatrix() mgl32.Mat4 {
	if e.modelMatrix != nil {
		return *e.modelMatrix
	}
	mat := e.translate.Mul4(e.scale).Mul4(e.rotate)
	e.modelMatrix = &mat
	return *e.modelMatrix
}

func (e *Entity) NormalMatrix() mgl32.Mat3 {
	if e.normalMatrix != nil {
		return *e.normalMatrix
	}
	mat := e.modelMatrix.Inv().Transpose().Mat3()
	e.normalMatrix = &mat
	return *e.normalMatrix
}

func (e *Entity) Material() material.Material {
	return material.PlasticRed
}
