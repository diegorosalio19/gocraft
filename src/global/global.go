package global

import (
	"io"
	"log"
	"os"
)

var Logger *log.Logger

type Setting struct {
	WindowWidth  int
	WindowHeight int
	Fullscreen   bool
}

var Settings Setting

type dummyOutput struct{}

func (_ dummyOutput) Write(_ []byte) (_ int, _ error) { return 0, nil }

func InitLogger(verbose bool) {
	var output io.Writer = os.Stdout
	if !verbose {
		output = dummyOutput{}
	}
	Logger = log.New(output, "", log.Lshortfile|log.Ltime)
}
