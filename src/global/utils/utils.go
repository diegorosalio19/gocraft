package utils

func MinInt(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func MaxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}

// InRange checks if a <= b < c
func InRange(a, b, c int) bool {
	return a <= b && b < c
}
