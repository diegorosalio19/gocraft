module bisonanon.com/gocraft

go 1.20

require (
	github.com/go-gl/gl v0.0.0-20211210172815-726fda9656d6
	github.com/go-gl/glfw v0.0.0-20221017161538-93cebf72946b
	github.com/go-gl/mathgl v1.0.0
	github.com/ojrac/opensimplex-go v1.0.2
)

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/nullboundary/glfont v0.0.0-20220309200611-a732e1465f7d // indirect
	golang.org/x/image v0.5.0 // indirect
)
