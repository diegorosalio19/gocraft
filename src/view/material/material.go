package material

import "github.com/go-gl/mathgl/mgl32"

type Materialer interface {
	Material() Material
}

type Material struct {
	Ambient   mgl32.Vec3
	Diffuse   mgl32.Vec3
	Specular  mgl32.Vec3
	Shininess float32
}

var PlasticBlack = Material{
	Ambient:   mgl32.Vec3{0.01, 0.01, 0.01},
	Diffuse:   mgl32.Vec3{0.01, 0.01, 0.01},
	Specular:  mgl32.Vec3{0.50, 0.50, 0.50},
	Shininess: 0.25,
}

var PlasticCyan = Material{
	Ambient:   mgl32.Vec3{0.0, 0.1, 0.06},
	Diffuse:   mgl32.Vec3{0.0, 0.50980392, 0.50980392},
	Specular:  mgl32.Vec3{0.50196078, 0.50196078, 0.50196078},
	Shininess: 0.25,
}

var PlasticGreen = Material{
	Ambient:   mgl32.Vec3{0.1, 0.35, 0.1},
	Diffuse:   mgl32.Vec3{0.1, 0.35, 0.1},
	Specular:  mgl32.Vec3{0.45, 0.55, 0.45},
	Shininess: 0.25,
}

var PlasticRed = Material{
	Ambient:   mgl32.Vec3{0.5, 0.0, 0.0},
	Diffuse:   mgl32.Vec3{0.5, 0.0, 0.0},
	Specular:  mgl32.Vec3{0.7, 0.6, 0.6},
	Shininess: 0.25,
}

var PlasticWhite = Material{
	Ambient:   mgl32.Vec3{0.55, 0.55, 0.55},
	Diffuse:   mgl32.Vec3{0.55, 0.55, 0.55},
	Specular:  mgl32.Vec3{0.70, 0.70, 0.70},
	Shininess: 0.25,
}

var PlasticYellow = Material{
	Ambient:   mgl32.Vec3{0.5, 0.5, 0.0},
	Diffuse:   mgl32.Vec3{0.5, 0.5, 0.0},
	Specular:  mgl32.Vec3{0.60, 0.60, 0.50},
	Shininess: 0.25,
}

var RubberGreen = Material{
	Ambient:   mgl32.Vec3{0.0, 0.05, 0.0},
	Diffuse:   mgl32.Vec3{0.4, 0.5, 0.4},
	Specular:  mgl32.Vec3{0.04, 0.7, 0.04},
	Shininess: .078125,
}

var Pearl = Material{
	Ambient:   mgl32.Vec3{0.25, 0.20725, 0.20725},
	Diffuse:   mgl32.Vec3{1, 0.829, 0.829},
	Specular:  mgl32.Vec3{0.296648, 0.296648, 0.296648},
	Shininess: 0.088,
}
