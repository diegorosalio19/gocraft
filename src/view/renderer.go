package view

import (
	"bisonanon.com/gocraft/view/lights"
	"bisonanon.com/gocraft/view/material"
	"bisonanon.com/gocraft/view/shaders/terrain"
	_ "embed"
)

type RenderManager struct {
	terrainShader terrain.Shader
	Cam           Camera
}

func NewRenderManager() RenderManager {
	return RenderManager{
		terrainShader: terrain.NewShader(),
		Cam:           NewCamera(),
	}
}

type Renderable interface {
	Mesher
	MatrixModeler
	material.Materialer
}

func (r *RenderManager) Render(entity Renderable) {
	r.terrainShader.Use()
	r.terrainShader.SetCameraPosition(r.Cam.Pos())
	r.terrainShader.SetMaterial(entity.Material())
	r.terrainShader.SetMatrix4f("uView", r.Cam.View())
	r.terrainShader.SetMatrix4f("uProjection", r.Cam.Perspective())
	r.terrainShader.SetMatrix4f("uModel", entity.ModelMatrix())
	r.terrainShader.SetMatrix3f("uNormalMatrix", entity.NormalMatrix())
	mesh := entity.Mesh()
	mesh.Draw()
}

func (r *RenderManager) SetLights(manager lights.Manager) {
	r.terrainShader.NoLight()
	r.terrainShader.SetAmbientStrength(0.1)
	r.terrainShader.SetDirectionLights(manager.Directions...)
	r.terrainShader.SetPointLights(manager.Points...)
	r.terrainShader.SetSpotLights(manager.Spots...)
}
