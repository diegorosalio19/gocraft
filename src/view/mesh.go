package view

import (
	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/mathgl/mgl32"
	"unsafe"
)

type Mesher interface {
	Mesh() Mesh
}

type MatrixModeler interface {
	ModelMatrix() mgl32.Mat4
	NormalMatrix() mgl32.Mat3
}

type Mesh struct {
	Vao, Vbo, Ebo    uint32
	EboStart, EboEnd int32
}

func (m *Mesh) Draw() {
	gl.BindVertexArray(m.Vao)
	gl.DrawElements(gl.TRIANGLES, m.EboEnd-m.EboStart, gl.UNSIGNED_INT, unsafe.Add(nil, m.EboStart))
}
