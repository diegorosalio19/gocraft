package gui

import (
	"bisonanon.com/gocraft/global"
	"github.com/nullboundary/glfont"
)

const (
	fontSize   = 20
	rowSpacing = 5
	leftMargin = 10
)

type Gui struct {
	textWriter *glfont.Font
	DebugInfos DebugInfo
}

func NewGui() Gui {
	textWriter, err := glfont.LoadFont("../assets/fonts/SourceSansPro-Regular.ttf", fontSize, global.Settings.WindowWidth, global.Settings.WindowHeight)
	if err != nil {
		return Gui{}
	}
	textWriter.SetColor(1, 1, 1, 1)
	g := Gui{textWriter: textWriter, DebugInfos: NewDebugInfo()}
	return g
}

func (g *Gui) RenderDebugInfos() {
	for i, row := range g.DebugInfos.rows {
		y := float32((i+1)*fontSize + rowSpacing)
		x := float32(leftMargin)
		for _, key := range row {
			val := g.DebugInfos.data[key]
			x += g.renderKeyValuePair(x, y, 1, key, *val) + leftMargin
		}
	}
}

func (g *Gui) renderKeyValuePair(x, y float32, scale float32, key string, val value) float32 {
	if val.colorMethod&Key == Key {
		g.textWriter.SetColor(val.color.X(), val.color.Y(), val.color.Z(), 1)
	} else {
		g.textWriter.SetColor(1, 1, 1, 1)
	}
	_ = g.textWriter.Printf(x, y, scale, key+": ")
	if val.colorMethod&Value == Value {
		g.textWriter.SetColor(val.color.X(), val.color.Y(), val.color.Z(), 1)
	} else {
		g.textWriter.SetColor(1, 1, 1, 1)
	}
	width := g.textWriter.Width(scale, key+": ")
	_ = g.textWriter.Printf(x+width, y, scale, val.text)
	return g.textWriter.Width(scale, val.text) + width
}
