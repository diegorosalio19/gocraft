package gui

import (
	"github.com/go-gl/mathgl/mgl32"
)

type ColorMethod = int

const (
	Key    = 1 << 0
	Value  = 1 << 1
	MaxRow = 10
)

type value struct {
	text        string
	color       mgl32.Vec3
	colorMethod ColorMethod
}

type DebugInfo struct {
	data map[string]*value
	rows [MaxRow][]string
}

func NewDebugInfo() DebugInfo {
	d := DebugInfo{}
	for i, _ := range d.rows {
		d.rows[i] = make([]string, 0)
	}
	d.data = make(map[string]*value)
	return d
}

func (d *DebugInfo) AddInfo(row int, key, val string, color mgl32.Vec3, method ColorMethod) {
	if row >= MaxRow {
		return
	}
	d.data[key] = &value{
		text:        val,
		color:       color,
		colorMethod: method,
	}
	d.rows[row] = append(d.rows[row], key)
}

type ValueModifier = func(v string)

func (d *DebugInfo) ValueModifier(key string) ValueModifier {
	val, ok := d.data[key]
	if !ok {
		return nil
	}
	p := &val.text
	return func(v string) {
		*p = v
	}
}
