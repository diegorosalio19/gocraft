#version 410

layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec3 aNormal;

uniform mat4 uModel;
uniform mat3 uNormalMatrix;
uniform mat4 uView;
uniform mat4 uProjection;

out vec3 fragPos;
out vec3 worldPos;
out vec3 normal;

void main() {
    gl_Position = uProjection * uView * uModel * vec4(aPosition, 1.0);
    fragPos = vec3(uModel * vec4(aPosition, 1.0));
    worldPos = fragPos;
    normal = normalize(uNormalMatrix * normalize(aNormal));
}
