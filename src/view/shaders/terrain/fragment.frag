#version 410

// Lights
#define MAX_DIRECTION_LIGHTS 32
#define MAX_POINT_LIGHTS 32
#define MAX_SPOT_LIGHTS 32

struct DirectionLight {
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct PointLight {
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};

struct SpotLight {
    vec3 position;
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;

    float cutOff;
    float outerCutOff;
};

struct Light {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform float uAmbientStrength;
uniform int uDirectionLightsNumber;
uniform DirectionLight uDirectionLights[MAX_DIRECTION_LIGHTS];
uniform int uPointLightsNumber;
uniform PointLight uPointLights[MAX_POINT_LIGHTS];
uniform int uSpotLightsNumber;
uniform SpotLight uSpotLights[MAX_SPOT_LIGHTS];

// Materials
struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

uniform Material uMaterial;
uniform vec3 uCameraPosition;

in vec3 fragPos;
in vec3 normal;

out vec4 fragColor;

float calcDiffFactor(vec3 lightDirection);
float calcSpecFactor(vec3 lightDirection, vec3 cameraPosition, float shininess);
vec3 calcMaterialColor(Material m, Light l);
Light calcDirectionLight(Material m, DirectionLight light);
Light calcPointLight(Material m, PointLight light);
Light calcSpotLight(Material m, SpotLight light);

void main() {
    vec3 finalColor = vec3(0);
    // DirectionLight
    for (int i = 0; i < uDirectionLightsNumber; i++){
        Light l = calcDirectionLight(uMaterial, uDirectionLights[i]);
        finalColor += calcMaterialColor(uMaterial, l);
    }
    // PointLights
    for (int i = 0; i < uPointLightsNumber; i++){
        Light l = calcPointLight(uMaterial, uPointLights[i]);
        finalColor += calcMaterialColor(uMaterial, l);
    }
    // SpotLights
    for (int i = 0; i < uSpotLightsNumber; i++){
        Light l = calcSpotLight(uMaterial, uSpotLights[i]);
        finalColor += calcMaterialColor(uMaterial, l);
    }
    fragColor = vec4(finalColor, 1.0);
}

Light calcDirectionLight(Material m, DirectionLight light){
    float diffFactor = calcDiffFactor(normalize(light.direction));
    float specFactor = calcSpecFactor(normalize(light.direction), uCameraPosition, m.shininess);
    Light result = { light.ambient, light.diffuse * diffFactor, light.specular * specFactor};
    return result;
}

Light calcPointLight(Material m, PointLight light){
    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance, light.quadratic * distance * distance);
    float diffFactor = calcDiffFactor(normalize(fragPos - light.position));
    float specFactor = calcSpecFactor(normalize(fragPos - light.position), uCameraPosition, m.shininess);
    Light result = { light.ambient * attenuation, light.diffuse * diffFactor * attenuation, light.specular * specFactor * attenuation};
    return result;
}

Light calcSpotLight(Material m, SpotLight light){
    float theta = dot(normalize(light.direction), normalize(fragPos - light.position));
    float epsilon = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff)/epsilon, 0.0, 1.0);
    if (theta > light.outerCutOff){
        PointLight l = { light.position, light.ambient, light.diffuse * intensity, light.specular * intensity, light.constant, light.linear, light.quadratic };
        Light l1 = calcPointLight(m, l);
        return l1;
    }
    Light result = {vec3(0), vec3(0), vec3(0)};
    return result;
}

float calcDiffFactor(vec3 lightDirection){
    return max(dot(normal, -lightDirection), 0.0);
}

float calcSpecFactor(vec3 lightDirection, vec3 cameraPosition, float shininess){
    vec3 viewDir = normalize(cameraPosition - fragPos);
    vec3 reflected = reflect(lightDirection, normal);
    return pow(max(dot(viewDir, reflected), 0.0), 16);
}

vec3 calcMaterialColor(Material m, Light l) {
    vec3 ambient0 = l.ambient * m.ambient * uAmbientStrength;
    vec3 diffuse0 = l.diffuse * m.diffuse;
    vec3 specular0 = l.specular * m.specular;
    return ambient0 + diffuse0 + specular0;
}
