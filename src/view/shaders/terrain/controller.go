package terrain

import (
	"bisonanon.com/gocraft/global/utils"
	"bisonanon.com/gocraft/view/lights"
	"bisonanon.com/gocraft/view/material"
	"bisonanon.com/gocraft/view/shaders"
	_ "embed"
	"fmt"
	"github.com/go-gl/mathgl/mgl32"
	"math"
)

const MaxLightsNumber = 32

//go:embed vertex.vert
var vertexShaderSource string

//go:embed fragment.frag
var fragmentShaderSource string

type Shader struct {
	shaders.Program
}

func NewShader() Shader {
	return Shader{
		shaders.NewProgramWithSource(vertexShaderSource, fragmentShaderSource, ""),
	}
}

func (p *Shader) SetAmbientStrength(strength float32) {
	p.SetFloats("uAmbientStrength", strength)
}

func (p *Shader) NoLight() {
	p.SetInts("uDirectionLightsNumber", 0)
	p.SetInts("uPointLightsNumber", 0)
	p.SetInts("uSpotLightsNumber", 0)
}

func (p *Shader) SetDirectionLights(ls ...lights.Direction) {
	l := utils.MinInt(len(ls), MaxLightsNumber)
	p.SetInts("uDirectionLightsNumber", int32(l))
	for i := 0; i < l; i++ {
		v := fmt.Sprintf("uDirectionLights[%d].", i)
		p.SetFloats(v+"direction", ls[i].Dir[:]...)
		p.SetFloats(v+"ambient", ls[i].Ambient[:]...)
		p.SetFloats(v+"diffuse", ls[i].Diffuse[:]...)
		p.SetFloats(v+"specular", ls[i].Specular[:]...)
	}
}

func (p *Shader) SetPointLights(ls ...lights.Point) {
	l := utils.MinInt(len(ls), MaxLightsNumber)
	p.SetInts("uPointLightsNumber", int32(l))
	for i := 0; i < l; i++ {
		v := fmt.Sprintf("uPointLights[%d].", i)
		p.SetFloats(v+"position", ls[i].Pos[:]...)
		p.SetFloats(v+"ambient", ls[i].Ambient[:]...)
		p.SetFloats(v+"diffuse", ls[i].Diffuse[:]...)
		p.SetFloats(v+"specular", ls[i].Specular[:]...)
		p.SetFloats(v+"constant", ls[i].Constant)
		p.SetFloats(v+"linear", ls[i].Linear)
		p.SetFloats(v+"quadratic", ls[i].Quadratic)
	}
}

func (p *Shader) SetSpotLights(ls ...lights.Spot) {
	l := utils.MinInt(len(ls), MaxLightsNumber)
	p.SetInts("uSpotLightsNumber", int32(l))
	for i := 0; i < l; i++ {
		v := fmt.Sprintf("uSpotLights[%d].", i)
		p.SetFloats(v+"position", ls[i].Pos[:]...)
		p.SetFloats(v+"direction", ls[i].Dir[:]...)
		p.SetFloats(v+"ambient", ls[i].Ambient[:]...)
		p.SetFloats(v+"diffuse", ls[i].Diffuse[:]...)
		p.SetFloats(v+"specular", ls[i].Specular[:]...)
		p.SetFloats(v+"constant", ls[i].Constant)
		p.SetFloats(v+"linear", ls[i].Linear)
		p.SetFloats(v+"quadratic", ls[i].Quadratic)
		p.SetFloats(v+"cutOff", float32(math.Cos(float64(mgl32.DegToRad(ls[i].CutOff)))))
		p.SetFloats(v+"outerCutOff", float32(math.Cos(float64(mgl32.DegToRad(ls[i].OuterCutOff)))))
	}
}

func (p *Shader) SetMaterial(m material.Material) {
	p.SetFloats("uMaterial.ambient", m.Ambient[:]...)
	p.SetFloats("uMaterial.diffuse", m.Diffuse[:]...)
	p.SetFloats("uMaterial.specular", m.Specular[:]...)
	p.SetFloats("uMaterial.shininess", m.Shininess)
}

func (p *Shader) SetCameraPosition(pos mgl32.Vec3) {
	p.SetFloats("uCameraPosition", pos[:]...)
}
