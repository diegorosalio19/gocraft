package shaders

import (
	"bisonanon.com/gocraft/global"
	"errors"
	"fmt"
	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/mathgl/mgl32"
	"os"
	"strings"
)

var currentProgram uint32

type Program struct {
	id uint32
}

func NewProgramWithPath(vertexShaderPath, fragmentShaderPath, geometryShaderPath string) Program {
	return NewProgramWithSource(readFile(vertexShaderPath), readFile(fragmentShaderPath), readFile(geometryShaderPath))
}

func NewProgramWithSource(vertexShaderSource, fragmentShaderSource, geometryShaderSource string) Program {
	logError := func(err error) {
		if err != nil {
			global.Logger.Println(err)
		}
	}
	id := gl.CreateProgram()
	logError(compileShader(id, vertexShaderSource+"\x00", gl.VERTEX_SHADER))
	logError(compileShader(id, fragmentShaderSource+"\x00", gl.FRAGMENT_SHADER))
	logError(compileShader(id, geometryShaderSource+"\x00", gl.GEOMETRY_SHADER))
	gl.LinkProgram(id)
	return Program{id: id}
}

func compileShader(id uint32, source string, shaderType uint32) error {
	if source == "\x00" {
		return errors.New("source empty")
	}
	shader := gl.CreateShader(shaderType)
	src, free := gl.Strs(source)
	gl.ShaderSource(shader, 1, src, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)
		logs := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(logs))
		return fmt.Errorf("failed to compile:\n%v", logs)
	}

	gl.AttachShader(id, shader)
	gl.DeleteShader(shader)
	return nil
}

func readFile(path string) string {
	contents, err := os.ReadFile(path)
	if err != nil {
		panic("reading file" + err.Error())
	}
	return string(contents)
}

func (p *Program) Use() {
	//if currentProgram == p.id {
	//	return
	//}
	gl.UseProgram(p.id)
	currentProgram = p.id
}

func (p *Program) SetInts(name string, value ...int32) {
	p.Use()
	location := gl.GetUniformLocation(p.id, gl.Str(name+"\x00"))
	switch len(value) {
	case 0:
		return
	case 1:
		gl.Uniform1i(location, value[0])
	case 2:
		gl.Uniform2i(location, value[0], value[1])
	case 3:
		gl.Uniform3i(location, value[0], value[1], value[2])
	default:
		gl.Uniform4i(location, value[0], value[1], value[2], value[3])
	}
}

func (p *Program) SetFloats(name string, value ...float32) {
	p.Use()
	location := gl.GetUniformLocation(p.id, gl.Str(name+"\x00"))
	switch len(value) {
	case 0:
		return
	case 1:
		gl.Uniform1f(location, value[0])
	case 2:
		gl.Uniform2f(location, value[0], value[1])
	case 3:
		gl.Uniform3f(location, value[0], value[1], value[2])
	default:
		gl.Uniform4f(location, value[0], value[1], value[2], value[3])
	}
}

func (p *Program) SetMatrix4f(name string, value mgl32.Mat4) {
	p.Use()
	location := gl.GetUniformLocation(p.id, gl.Str(name+"\x00"))
	gl.UniformMatrix4fv(location, 1, false, &value[0])
}

func (p *Program) SetMatrix3f(name string, value mgl32.Mat3) {
	p.Use()
	location := gl.GetUniformLocation(p.id, gl.Str(name+"\x00"))
	gl.UniformMatrix3fv(location, 1, false, &value[0])
}
