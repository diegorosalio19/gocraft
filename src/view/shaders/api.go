package shaders

import (
	"bisonanon.com/gocraft/view/lights"
	"bisonanon.com/gocraft/view/material"
	"github.com/go-gl/mathgl/mgl32"
)

type Lighter interface {
	SetAmbientStrength(strength float32)
	NoLight()
	SetDirectionLights(ls ...lights.Direction)
	SetPointLights(ls ...lights.Point)
	SetSpotLights(ls ...lights.Spot)
	SetCameraPosition(pos mgl32.Vec3)
	SetMaterial(m material.Material)
}
