package lights

type Manager struct {
	Directions []Direction
	Points     []Point
	Spots      []Spot
}

func NewManger() Manager {
	return Manager{
		Directions: make([]Direction, 0),
		Points:     make([]Point, 0),
		Spots:      make([]Spot, 0),
	}
}
