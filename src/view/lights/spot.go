package lights

import "github.com/go-gl/mathgl/mgl32"

type Spot struct {
	Point
	Dir         mgl32.Vec3
	CutOff      float32
	OuterCutOff float32
}
