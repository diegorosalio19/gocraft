package lights

import "github.com/go-gl/mathgl/mgl32"

type Direction struct {
	LightColor
	Dir mgl32.Vec3
}

var Sun = Direction{
	LightColor: WhiteLight,
	Dir:        mgl32.Vec3{1, -1, 1}.Normalize(),
}
