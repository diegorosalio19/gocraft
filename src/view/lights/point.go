package lights

import "github.com/go-gl/mathgl/mgl32"

type Point struct {
	LightColor
	Attenuation
	Pos mgl32.Vec3
}
