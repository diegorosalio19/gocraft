package lights

import "github.com/go-gl/mathgl/mgl32"

type Color mgl32.Vec3

type LightColor struct {
	Ambient  Color
	Diffuse  Color
	Specular Color
}

var WhiteLight = LightColor{
	Ambient:  Color{1, 1, 1},
	Diffuse:  Color{1, 1, 1},
	Specular: Color{1, 1, 1},
}

type Attenuation struct {
	Constant  float32
	Linear    float32
	Quadratic float32
}

var (
	D7    = Attenuation{Constant: 1, Linear: 0.7, Quadratic: 1.8}
	D13   = Attenuation{Constant: 1, Linear: 0.35, Quadratic: 0.44}
	D20   = Attenuation{Constant: 1, Linear: 0.22, Quadratic: 0.20}
	D32   = Attenuation{Constant: 1, Linear: 0.14, Quadratic: 0.07}
	D50   = Attenuation{Constant: 1, Linear: 0.09, Quadratic: 0.032}
	D65   = Attenuation{Constant: 1, Linear: 0.07, Quadratic: 0.017}
	D100  = Attenuation{Constant: 1, Linear: 0.045, Quadratic: 0.0075}
	D160  = Attenuation{Constant: 1, Linear: 0.027, Quadratic: 0.0028}
	D200  = Attenuation{Constant: 1, Linear: 0.022, Quadratic: 0.0019}
	D325  = Attenuation{Constant: 1, Linear: 0.014, Quadratic: 0.0007}
	D600  = Attenuation{Constant: 1, Linear: 0.007, Quadratic: 0.0002}
	D3250 = Attenuation{Constant: 1, Linear: 0.0014, Quadratic: 0.000007}
)
