package view

import (
	"bisonanon.com/gocraft/global"
	"github.com/go-gl/mathgl/mgl32"
	"math"
)

type Camera struct {
	cen, worldUp, pos, dir, front, right mgl32.Vec3
	yaw, pitch                           float32
	lockedToCenter                       bool
	speed                                float32
	sense                                float32
	fov                                  float32
	zoomSpeed                            float32
}

func NewCamera() Camera {
	return Camera{
		worldUp:   mgl32.Vec3{0, 1, 0},
		yaw:       -90,
		speed:     10,
		sense:     0.1,
		zoomSpeed: 5,
		fov:       80,
	}
}

func (c *Camera) View() mgl32.Mat4 {
	var lookingAt mgl32.Vec3
	if c.lockedToCenter {
		lookingAt = c.cen
	} else {
		lookingAt = c.pos.Add(c.front)
	}

	return mgl32.LookAt(
		c.pos.X(), c.pos.Y(), c.pos.Z(),
		lookingAt.X(), lookingAt.Y(), lookingAt.Z(),
		c.worldUp.X(), c.worldUp.Y(), c.worldUp.Z(),
	)
}

func (c *Camera) Perspective() mgl32.Mat4 {
	return mgl32.Perspective(mgl32.DegToRad(c.fov), float32(global.Settings.WindowWidth)/float32(global.Settings.WindowHeight), 0.1, 200)
}

func (c *Camera) Move(movement mgl32.Vec3) {
	c.pos = c.pos.Add(c.right.Mul(c.speed * movement.X()))
	c.pos = c.pos.Add(c.worldUp.Mul(c.speed * movement.Y()))
	c.pos = c.pos.Sub(c.dir.Mul(c.speed * movement.Z()))
}

func (c *Camera) Rotate(rotation mgl32.Vec2) {
	c.yaw += rotation.X() * c.sense
	c.pitch += rotation.Y() * c.sense
	if c.pitch > 89.0 {
		c.pitch = 89.0
	}
	if c.pitch < -89.0 {
		c.pitch = -89.0
	}
	c.updateVectors()
}

func (c *Camera) ZoomOut() {
	c.fov += c.zoomSpeed
	if c.fov > 90 {
		c.fov = 90
	}
}

func (c *Camera) ZoomIn() {
	c.fov -= c.zoomSpeed
	if c.fov < 30 {
		c.fov = 30
	}
}

func (c *Camera) LockToPoint(point mgl32.Vec3) {
	c.lockedToCenter = true
	c.cen = point
}

func (c *Camera) Unlock() {
	c.lockedToCenter = false
}

func (c *Camera) Pos() mgl32.Vec3 {
	return c.pos
}

func (c *Camera) updateVectors() {
	c.front = mgl32.Vec3{
		float32(math.Cos(float64(mgl32.DegToRad(c.yaw))) * math.Cos(float64(mgl32.DegToRad(c.pitch)))),
		float32(math.Sin(float64(mgl32.DegToRad(c.pitch)))),
		float32(math.Sin(float64(mgl32.DegToRad(c.yaw))) * math.Cos(float64(mgl32.DegToRad(c.pitch)))),
	}.Normalize()
	c.right = c.front.Cross(c.worldUp).Normalize()
	c.dir = c.worldUp.Cross(c.right).Normalize()
}
